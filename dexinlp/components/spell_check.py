import enchant
from enchant.checker import SpellChecker
from dexinlp import BasePipelineComponent

class EnchantComponent(BasePipelineComponent):

    required_input_keys = ["query"]

    def _process(self, input_dict):
        query = input_dict["query"]
        corrected_spelling = self.correct_spelling(query)
        corrected = corrected_spelling.lower() != query.lower()
        return {"correctedText": {"value": corrected_spelling, "isCorrected": corrected}}

    def correct_spelling(self, text):
        chkr = enchant.checker.SpellChecker("en_US")
        chkr.set_text(text)
        for err in chkr:
            sug = err.suggest()[0]
            err.replace(sug)

        corrected = chkr.get_text()
        return corrected
