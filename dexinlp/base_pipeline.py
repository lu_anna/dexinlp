class Pipeline:
    """Pipeline class that processes a user's input query and enriches it
       with more data to return to the client.
    """

    def __init__(self, components):
        """Initialize the pipeline with components used for processing
           a user's input query.

        Arguments:
            components {array} -- List iof components to be used by the pipeline.
        """
        self._components = components

    def run_pipeline(self, input_text):
        """Run the processing pipeline witb the user's query.

        Arguments:
            input_text {string} -- Input text from the user.

        Returns: dict -- a dictionary enriched with the data provided by each
        component in the pipeline.
        """
        input_dict = {"query": input_text}
        for component in self._components:
            new_values_dict = component.process(input_dict)
            input_dict.update(new_values_dict)
        return input_dict
