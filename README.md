# DEXi NLP

This package contains base and abstract classes 

Example items that belong in this repo:
 - Abstract classes inherited by any customer-specific components
 - Customer-agnostic concrete classes that can be reused in any DEXi deployment
 
Example items that do not:
  - Anything specific to or dependent on a customer schema
  
A typical use is to `import dexinlp` into a customer web application.
