from dexinlp import BasePipelineComponent
from dexinlp import Pipeline
from dexinlp.components.spell_check import EnchantComponent 
import pytest

class DummyComponent(BasePipelineComponent):

    def _process(self, input_dict):
        return {}

class ReverseStringComponent(BasePipelineComponent):

    required_input_keys = ['query']

    def _process(self, input_dict):
        query = input_dict["query"]
        return {"reversed": query[::-1]}



class TestComponent:

    def test_dummy_component(self):
        input_dict = {}
        expected = {}
        assert expected == DummyComponent().process(input_dict)

    def test_reversed_string_component(self):
        query = "my name is john"
        input_dict = {"query": query}
        expected = {"reversed": "nhoj si eman ym"}
        reverse_component = ReverseStringComponent()
        assert expected == reverse_component.process(input_dict)
        input_dict_more_data = {"query": query, "more_data": [1, 2]}
        assert expected == reverse_component.process(input_dict_more_data)
    
    def test_missing_input_data(self):
        input_dict = {}
        reverse_component = ReverseStringComponent()
        with pytest.raises(KeyError):
            reverse_component.process(input_dict)


class TestPipeline:

    def test_empty_pipeline(self):
        empty_pipeline = Pipeline([])
        query = "hello"
        expected = {"query": "hello"}
        assert expected == empty_pipeline.run_pipeline(query)

    def test_one_component_pipeline(self):
        reverse_component = ReverseStringComponent()
        pipeline = Pipeline([reverse_component])
        query = "hello"
        expected = {"query": "hello", "reversed": "olleh"}
        assert expected == pipeline.run_pipeline(query)

    class TestEnchantComponent:

        def test_misspeling(self):
            component = EnchantComponent()
            input_dict = { "query": "my friend has a relativve with schitzophrennia" }
            data_dict = component.process(input_dict)
            expected = {"correctedText": {"value": "my friend has a relative with schizophrenia", 
                        "isCorrected": True}}
            assert expected == data_dict

            input_dict = { "query": "he seems to have manicc depressivee disordr"}
            data_dict = component.process(input_dict)
            expected = {"correctedText": {"value": "he seems to have manic depressive disorder", 
                        "isCorrected": True}}
        
        def test_no_mispelling(self):
            component = EnchantComponent()
            input_dict = { "query": "my friend has a relative with schizophrenia" }
            data_dict = component.process(input_dict)
            expected = {"correctedText": {"value": "my friend has a relative with schizophrenia", 
                        "isCorrected": False}}
            assert expected == data_dict

        def test_empty_string(self):
            component = EnchantComponent()
            input_dict = { "query": "" }
            data_dict = component.process(input_dict)
            expected = {"correctedText": {"value": "", 
                        "isCorrected": False}}
            assert expected == data_dict
